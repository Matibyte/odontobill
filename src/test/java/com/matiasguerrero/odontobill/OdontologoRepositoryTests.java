package com.matiasguerrero.odontobill;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;


import org.springframework.test.annotation.Rollback;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.junit.jupiter.api.Order;

import com.matiasguerrero.odontobill.model.dao.OdontologoRepository;
import com.matiasguerrero.odontobill.model.entity.Odontologo;



@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@RunWith(JUnitPlatform.class)
@TestMethodOrder(OrderAnnotation.class)
class OdontologoRepositoryTests {
	
	@Autowired
    private OdontologoRepository odontologoRepository;
	
	//TEST CREATE
	@Test
	@Rollback(false)
	@Order(1)
	public void testCreateOdontologo() {
	    Odontologo savedOdontologo = odontologoRepository.save(new Odontologo("Juan Cruz", "OD987654"));
	     
	    assertThat(savedOdontologo.getId()).isGreaterThan(0);
	}
	
	//TEST UPDATE
	@Test
	@Rollback(false)
	@Order(4)
	public void testUpdateOdontologo() {
		Odontologo odontologo = odontologoRepository.findByMatricula("OD987654");
		odontologo.setNombre("Ana Maria");
	     
		odontologoRepository.save(odontologo);
	     
	    Odontologo updatedOdontologo = odontologoRepository.findByMatricula("OD987654");
	     
	    assertThat(updatedOdontologo.getNombre()).isEqualTo("Ana Maria");
	}
	
	//TEST DELETE
	@Test
	@Rollback(false)
	@Order(5)
	public void testDeleteOdontologo() {
		Odontologo odontologo = odontologoRepository.findByMatricula("OD987654");
	     
		odontologoRepository.deleteById(odontologo.getId());
	     
	    Odontologo deletedOdontologo = odontologoRepository.findByMatricula("OD987654");
	     
	    assertThat(deletedOdontologo).isNull();       
	}
	
	//TEST FINDBYMATRICULA
	@Test
	@Order(2)
	public void testFindOdontologoByMatricula() {
		Odontologo odontologo = odontologoRepository.findByMatricula("OD987654");    
		assertThat(odontologo.getMatricula()).isEqualTo("OD987654");
	}
			
	//TEST FINDALL
	@Test
	@Order(3)
	public void testListOdontologos() {
		List<Odontologo> odontologos = (List<Odontologo>) odontologoRepository.findAll();
		assertThat(odontologos).size().isGreaterThan(0);
	}
	
}
