package com.matiasguerrero.odontobill;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.matiasguerrero.odontobill.model.dao.PacienteRepository;
import com.matiasguerrero.odontobill.model.entity.Paciente;

@SpringBootTest
@RunWith(JUnitPlatform.class)
class PacienteRepositoryTest {
	
	@Autowired
	private PacienteRepository pacienteRepository;
	
	private Paciente paciente;
	
	@BeforeEach
	void setUp() throws Exception {	
		paciente = new Paciente(37304765, "Matias");
		
	}

	@AfterEach
	void tearDown() throws Exception {
		paciente = null;
	}
	
	@Test
	void testBuscarPorNombre() {
		Paciente pacienteGuardado = pacienteRepository.save(paciente);
		Paciente pruebaPaciente = pacienteRepository.findByNombre("Matias");
		assertNotNull(pruebaPaciente);
		
		pacienteRepository.delete(pacienteGuardado);
		
	}
}
