package com.matiasguerrero.odontobill.layer.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.matiasguerrero.odontobill.model.dto.FacturaDTO;
import com.matiasguerrero.odontobill.model.dto.PacienteDTO;
import com.matiasguerrero.odontobill.model.entity.Factura;
import com.matiasguerrero.odontobill.model.entity.Paciente;
import com.matiasguerrero.odontobill.model.service.IFacturaService;

@RestController
@RequestMapping("/api/factura")
public class FacturaController {

private static final Logger log = LoggerFactory.getLogger(PacienteController.class);
	
	@Autowired
	private IFacturaService facturaService;
	
	
	@PostMapping("/save")
	public ResponseEntity<Object> save(@RequestBody Factura factura) {
		try {
			log.info("Creando Cliente...");
			facturaService.crearFactura(factura);
		}catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(factura, HttpStatus.OK);
	}
}
