package com.matiasguerrero.odontobill.layer.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.matiasguerrero.odontobill.model.dto.PacienteDTO;
import com.matiasguerrero.odontobill.model.entity.Paciente;
import com.matiasguerrero.odontobill.model.service.IPacienteService;

@RestController
@RequestMapping("/api/paciente")
public class PacienteController {
	
	private static final Logger log = LoggerFactory.getLogger(PacienteController.class);
	
	@Autowired
	private IPacienteService pacienteService;
	
	//@CrossOrigin(origins="*")
	@GetMapping("/findAll")
	public ResponseEntity<List<Paciente>> buscarTodos(){
		List<Paciente> lista;
		try {
			log.info("Listado de Clientes...");
			lista = pacienteService.findAll();
		}catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/findByName/{nombre}")
	public ResponseEntity<Object> buscarPorNombre(@PathVariable("nombre") String nombre){
		PacienteDTO pacienteDTO = new PacienteDTO();
		Paciente paciente = pacienteService.findByNombre(nombre);
		if(paciente == null ) {
            return new ResponseEntity<>("Error: No existe tal Paciente", HttpStatus.CONFLICT);
        }
		try {
			log.info("Buscando Cliente...");
			pacienteDTO = pacienteService.convertToPacienteDTO(paciente);
		}catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(pacienteDTO, HttpStatus.OK);
	}
	
	/* OTRA ALTERNATIVA DESCARTADA
	@GetMapping("/nombre={nombre}")
	public ResponseEntity<Map<String,Object>> buscarPorNombre(@PathVariable("nombre") String nombre){
		Map<String, Object> pacienteDTO = new LinkedHashMap<>();
		try {
			log.info("Listado de Clientes...");
			pacienteDTO = pacienteService.findByNombreDTO(nombre);
		}catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(pacienteDTO, HttpStatus.OK);
	}
	*/
	@PostMapping("/save")
	public ResponseEntity<Object> save(@RequestBody PacienteDTO pacienteDTO) {
		Paciente paciente = pacienteService.findByNombre(pacienteDTO.getNombrePaciente());
		if(paciente != null) {
            return new ResponseEntity<>("Error: El paciente ya existe", HttpStatus.CONFLICT);
        }
		try {
			log.info("Creando Cliente...");
			pacienteService.crearPaciente(pacienteDTO);
		}catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(pacienteDTO, HttpStatus.OK);
	}
	
	@PutMapping("/modify")
	public ResponseEntity<Object> modify(@RequestBody PacienteDTO pacienteDTO) {
		Paciente paciente = pacienteService.findByNombre(pacienteDTO.getNombrePaciente());
		if(paciente == null) {
            return new ResponseEntity<>("Error: El paciente que intentas modificar no existe", HttpStatus.CONFLICT);
        }
		try {
			log.info("Modificando Cliente...");
			pacienteService.actualizarPaciente(pacienteDTO);
		}catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(pacienteDTO, HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{nombre}")
	public ResponseEntity<Object> eliminar(@PathVariable("nombre") String nombre) {
		System.out.print(nombre);
		Paciente paciente = pacienteService.findByNombre(nombre);
		if(paciente == null) {
            return new ResponseEntity<>("Error: El paciente que intentas eliminar ni siquiera existe", HttpStatus.CONFLICT);
        }
		try {
			log.info("Eliminando Cliente...");
			pacienteService.eliminar(paciente);
		}catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>("Paciente eliminado", HttpStatus.OK);
		
	}
	/*
	@PutMapping("/modify2")
	public Paciente modificar2(@RequestBody Paciente paciente) {
		pacienteService.actualizarPacienteDos(paciente);
		return paciente;
	}
	*/
}
