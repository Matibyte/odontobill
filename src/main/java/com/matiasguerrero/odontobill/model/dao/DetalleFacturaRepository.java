package com.matiasguerrero.odontobill.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.matiasguerrero.odontobill.model.entity.DetalleFactura;

public interface DetalleFacturaRepository extends JpaRepository<DetalleFactura, Long> {

}
