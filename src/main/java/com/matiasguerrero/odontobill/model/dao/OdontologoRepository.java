package com.matiasguerrero.odontobill.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.matiasguerrero.odontobill.model.entity.Odontologo;

public interface OdontologoRepository extends JpaRepository<Odontologo, Long> {

	Odontologo findByMatricula(String matricula);
}
