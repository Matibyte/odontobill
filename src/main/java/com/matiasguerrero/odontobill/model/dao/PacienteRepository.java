package com.matiasguerrero.odontobill.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.matiasguerrero.odontobill.model.entity.Paciente;

public interface PacienteRepository extends JpaRepository<Paciente, Long> {
	
	 Paciente findByNombre(String nombre);
}
