package com.matiasguerrero.odontobill.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.matiasguerrero.odontobill.model.entity.Tratamiento;

public interface TratamientoRepository extends JpaRepository<Tratamiento, Long> {

	Tratamiento findByNombre(String nombre);
}
