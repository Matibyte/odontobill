package com.matiasguerrero.odontobill.model.entity;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Tratamientos")
public class Tratamiento {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String	nombre;
	
	private Double costo;

	//Constructores
	public Tratamiento() {
	}

	public Tratamiento(String nombre, Double costo) {
		this.nombre = nombre;
		this.costo = costo;
	}
	
	//Getters and Setters
	public Long getId() {
		return id;
	}
	

	public void setId(Long id) {
		this.id = id;
	}
	

	public String getNombre() {
		return nombre;
	}

	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	

	
	public Double getCosto() {
		return costo;
	}
	

	public void setCosto(Double costo) {
		this.costo = costo;
	}

	
	
}
