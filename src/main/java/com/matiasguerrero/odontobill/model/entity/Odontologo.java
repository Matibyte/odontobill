package com.matiasguerrero.odontobill.model.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Odontologos")
public class Odontologo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String	nombre;
	
	@Column(name = "matricula_profesional")
	private String matricula;
	
	//Con EAGER establecemos que cada vez que consultamos por un odontologo la base 
	//tambien hace otra consulta trayendo todas las facturas asociadas al odontologo
	@OneToMany(mappedBy = "odontologo", fetch = FetchType.EAGER)
	private List<Factura> facturas;

	//Constructores
	public Odontologo() {
	}

	public Odontologo(String nombre, String matricula) {
		this.nombre = nombre;
		this.matricula = matricula;
	}

	//Getters and Setters
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public List<Factura> getFacturas() {
		return facturas;
	}

	public void setFacturas(List<Factura> facturas) {
		this.facturas = facturas;
	}
		

}
