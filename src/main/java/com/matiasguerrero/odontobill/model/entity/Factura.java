package com.matiasguerrero.odontobill.model.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Facturas")
public class Factura {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	//@Transient: para q el atributo sea ignorado durante la persistencia
	private Date fecha;
	
	@ManyToOne
	@JoinColumn(name = "idPaciente")
	private Paciente paciente;
	
	@ManyToOne
	@JoinColumn(name = "idOdontologo")
	private Odontologo odontologo;
	
	@Column(name = "importe_total")
	private Double importe;
	
	@OneToMany(mappedBy = "factura", cascade = CascadeType.ALL)
	private List<DetalleFactura> detalles;

	//Constructores
	public Factura() {
	}

	public Factura(Paciente paciente, Odontologo odontologo, Double importe) {
		this.paciente = paciente;
		this.odontologo = odontologo;
		this.importe = importe;
	}

	
	public Long getId() {
		return id;
	}
	
	//Getters and Setters

	public void setId(Long id) {
		this.id = id;
	}
	

	public Date getFecha() {
		return fecha;
	}
	

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	

	public Double getImporte() {
		return importe;
	}
	

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	
	public List<DetalleFactura> getDetalles() {
		return detalles;
	}

	
	public void setDetalles(List<DetalleFactura> detalles) {
		this.detalles = detalles;
	}

	
	
}
