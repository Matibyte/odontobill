package com.matiasguerrero.odontobill.model.service;

import com.matiasguerrero.odontobill.model.dto.FacturaDTO;
import com.matiasguerrero.odontobill.model.entity.Factura;

public interface IFacturaService {

	void crearFactura(Factura factura);
}
