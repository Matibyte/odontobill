package com.matiasguerrero.odontobill.model.service.implementation;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.matiasguerrero.odontobill.model.dao.PacienteRepository;
import com.matiasguerrero.odontobill.model.dto.PacienteDTO;
import com.matiasguerrero.odontobill.model.entity.Paciente;
import com.matiasguerrero.odontobill.model.service.IPacienteService;

@Service
public class PacienteServiceImplementation implements IPacienteService {
	
	@Autowired
	private PacienteRepository pacienteRepository;

	@Override
	public List<Paciente> findAll() {
		// Aqui implemntar el patron DTO para cada metodo
		return pacienteRepository.findAll();
	}
	
	@Override
	public Paciente findByNombre(String nombre) {
		return pacienteRepository.findByNombre(nombre);
	}
	
	/*
	@Override
	public Map<String, Object> buscarPorNombre(String nombre){
		// TODO Auto-generated method stub
		System.out.println(nombre);
		Paciente paciente = pacienteRepository.findByNombre(nombre);
		System.out.println(paciente);
		Map<String, Object> dto = new LinkedHashMap<>();
		dto.put("Nombre", paciente.getNombre());
		dto.put("Dni", paciente.getDni());
		dto.put("Email", paciente.getEmail());
		return dto;
	}
	*/
	
	@Override
	public void crearPaciente(PacienteDTO pacienteDTO) {
		// TODO Auto-generated method stub
		Paciente paciente = convertToPaciente(pacienteDTO);
		pacienteRepository.save(paciente);
	}

	@Override
	public void actualizarPaciente(PacienteDTO pacienteDTO) {
		// TODO Auto-generated method stub
		Paciente paciente = convertToPaciente(pacienteDTO);
		//sin la sig sentencia se crea otra copia en la bd en vez de modificar el registro original
		paciente.setId(pacienteRepository.findByNombre(paciente.getNombre()).getId());	
		pacienteRepository.save(paciente);
	}
	
	@Override
	public void eliminar(Paciente paciente) {
		// TODO Auto-generated method stub
		pacienteRepository.deleteById(paciente.getId());
	}

	
	@Override
	public Paciente convertToPaciente(PacienteDTO pacienteDTO) {
		Paciente paciente = new Paciente();
		paciente.setNombre(pacienteDTO.getNombrePaciente());
		paciente.setDni(pacienteDTO.getDni());
		paciente.setEmail(pacienteDTO.getCorreo());
		
		return paciente;
	}
	
	@Override
	public PacienteDTO convertToPacienteDTO(Paciente paciente){
		// TODO Auto-generated method stub
		PacienteDTO pacienteDTO = new PacienteDTO();
		pacienteDTO.setNombrePaciente(paciente.getNombre());
		pacienteDTO.setDni(paciente.getDni());
		pacienteDTO.setCorreo(paciente.getEmail());;
		
		return pacienteDTO;
	}
	
	/*
	@Override
	public void actualizarPacienteDos(Paciente paciente) {
		// TODO Auto-generated method stub
		pacienteRepository.save(paciente);
	}
	*/
}
