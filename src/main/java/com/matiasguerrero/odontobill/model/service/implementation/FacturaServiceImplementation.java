package com.matiasguerrero.odontobill.model.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.matiasguerrero.odontobill.model.dao.FacturaRepository;
import com.matiasguerrero.odontobill.model.dto.FacturaDTO;
import com.matiasguerrero.odontobill.model.dto.PacienteDTO;
import com.matiasguerrero.odontobill.model.entity.Factura;
import com.matiasguerrero.odontobill.model.entity.Paciente;
import com.matiasguerrero.odontobill.model.service.IFacturaService;

@Service
public class FacturaServiceImplementation implements IFacturaService {

	@Autowired
	private FacturaRepository facturaRepository;
	
	@Override
	public void crearFactura(Factura factura){
		// TODO Auto-generated method stub
		//Factura facutra = convertToFactura(facturaDTO); 
		facturaRepository.save(factura);
	}
	/*
	@Override
	public Factura convertToFactura(Factura factura) {
		Factura facutra = new Factura();
		
		//facutra.setNombre(pacienteDTO.getNombrePaciente());
		//facutra.setDni(pacienteDTO.getDni());
		//facutra.setEmail(pacienteDTO.getCorreo());
		
		return facutra;
	}
	*/
}
