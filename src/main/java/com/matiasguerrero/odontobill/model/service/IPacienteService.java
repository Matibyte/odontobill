package com.matiasguerrero.odontobill.model.service;

import java.util.List;

import com.matiasguerrero.odontobill.model.dto.PacienteDTO;
import com.matiasguerrero.odontobill.model.entity.Paciente;

public interface IPacienteService {
	
	List<Paciente> findAll();
	
	Paciente findByNombre(String nombre);
		
	void crearPaciente(PacienteDTO pacienteDTO);
	
	void actualizarPaciente(PacienteDTO pacienteDTO);
	
	void eliminar(Paciente paciente);
	
	PacienteDTO convertToPacienteDTO(Paciente paciente);
	
	Paciente convertToPaciente(PacienteDTO pacienteDTO);
	
	//void actualizarPacienteDos(Paciente paciente);
}
