package com.matiasguerrero.odontobill;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OdontobillApplication {

	public static void main(String[] args) {
		SpringApplication.run(OdontobillApplication.class, args);
	}

}
